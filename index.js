'use strict';

//define variables, please, use relative paths
var deposito1 = "../DEPOSITO";
var deposito2 = "../DEPOSITO2";
var VirtualPath = "../Animes";

//dependences
var chokidar = require('chokidar');
var path = require('path');
var fs = require('fs');

//Start
var watcher = chokidar.watch(deposito1, {
    ignorePermissionErrors: true,
    persistent: true
});
watcher.add(deposito2);

//define actions
watcher.on('addDir', function(Fpath) {
    linkdir(Fpath);

}).on('unlinkDir', function(Fpath) {
    unlinkDir(Fpath);
});

//functions
function linkdir(Fpath) {
    if (Fpath == deposito1 || Fpath == deposito2) {
        return;
    }
    console.log('"' + Fpath + '" directory added.');
    var out = VirtualPath + '/' + path.basename(Fpath);
	var exists = false;
	var stats = verifyDir(out);
	if (stats[0]) {
        if (stats[1] == "link") {
            console.log('"' + Fpath + '" have a path link, ignoring...');
            return;
        } else {
            console.log('"' + Fpath + '" is a phisical directory, ignoring...');
            return;
        }
    } else {
		if (stats[1] == "link"){
		fs.unlink(out);
			if (fs.existsSync(out)) {
				console.log("fail to unlink the corrupted link \"" + lnk + "\" directory");
			} else {
				console.log("\"" + out + "\" is a invalid link, fixing...");
			}
		}
        fs.symlinkSync(Fpath, out, "dir");
        stats = fs.lstatSync(out);
        if (fs.existsSync(out)) {
            if (!stats.isSymbolicLink()) {
                console.log("fail to link the " + out + " directory");
            }
        }
    }
}

function unlinkDir(Fpath) {
    var dirname = path.basename(Fpath);
    var lnk = VirtualPath + '/' + dirname;
    var stats;
    if (path.isAbsolute(Fpath)) {
        Fpath = path.relative(__dirname, Fpath);
    }
    console.log('"' + Fpath + '" directory removed.');
    if (Fpath.replace(/\\/g, '/') == deposito1 + '/' + dirname) {
        if (fs.existsSync(deposito2 + '/' + dirname)) {
			stats = verifyDir(lnk);
            if (stats[1] == "link") {
                fs.unlinkSync(lnk);
                if (fs.existsSync(lnk)) {
                    console.log("fail to unlink the \"" + lnk + "\" directory");
                }
            } else {
				if (stats[0]){
					console.log('"' + Fpath + '" is a phisical directory, ignoring...');
					return;
				}
			}
            fs.symlinkSync(deposito2 + '/' + dirname, lnk, "dir");
            if (fs.existsSync(lnk)) {
                stats = fs.lstatSync(lnk);
                if (!stats.isSymbolicLink()) {
                    console.log("fail to link the \"" + lnk + "\" directory");
                }
                console.log("Directory \"" + Fpath + "\" moved, link updated.");
            }
            return;
        }
    } else {
        if (fs.existsSync(deposito1 + '/' + dirname)) {
			stats = verifyDir(lnk);
            if (stats[1] == "link") {
                fs.unlinkSync(lnk);
                if (fs.existsSync(lnk)) {
                    console.log("fail to unlink the \"" + lnk + "\" directory");
                }
            } else {
				if (stats[0]){
					console.log('"' + Fpath + '" is a phisical directory, ignoring...');
					return;
				}
			}
            fs.symlinkSync(deposito1 + '/' + dirname, lnk, "dir");
            if (fs.existsSync(lnk)) {
                stats = fs.lstatSync(lnk);
                if (!stats.isSymbolicLink()) {
                    console.log("fail to link the \"" + lnk + "\" directory");
                }
                console.log("Directory \"" + Fpath + "\" moved, link updated.");
            }
            return;
        }
    }
    stats = verifyDir(lnk);
	if (stats[0] == false && stats[1] == "dir"){
		return;
	}
    stats = fs.lstatSync(lnk);
    if (!stats.isSymbolicLink()) {
        console.log('"' + Fpath + '" not is a virtual path, ignoring...');
        return;
    }
    fs.unlink(lnk);
    if (fs.existsSync(lnk)) {
        console.log("fail to unlink the " + lnk + " directory");
    }
}

function verifyDir(dir){
	if (fs.existsSync(dir)){
		stats = fs.lstatSync(dir);
		if (stats.isSymbolicLink()){
			return [true, "link"];
		} else {
			return [true, "dir"];
		}
	} else {
		try{
			var stats = fs.lstatSync(dir);
			return [false, "link"];
		} catch(exp) {
			return [false, "dir"];
		}
	}
}